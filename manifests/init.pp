class acrc_repos (
  $update_cmd = $acrc_repos::params::update_cmd,
  $update_message = $acrc_repos::params::update_message,
  $apply_updates = $acrc_repos::params::apply_updates
) inherits ::acrc_repos::params {
    if $osfamily == 'RedHat' {
      $yumrepos = hiera_hash('repo::yum')
      if $yumrepos {
          create_resources('yumrepo', $yumrepos)
      }

      package { 'yum-cron':
        ensure => present,
      }

      service { 'yum-cron':
        ensure     => running,
        hasstatus  => true,
        hasrestart => true,
        require    => Package['yum-cron'],
      }

      file { '/etc/yum/yum-cron.conf':
        ensure  => file,
        content => template('acrc_repos/yum-cron.conf.erb'),
      }

  }
}
